module.exports = {
  cacheDirectory: "node_modules/.cache/jest",
  collectCoverageFrom: ["**/*.{ts,tsx}", "!**/*.d.ts", "!**/node_modules/**"],
  moduleNameMapper: { "^.+\\.(css|sass|scss)$": "<rootDir>/src/mocks/style-mock.js" },
  setupFilesAfterEnv: ["<rootDir>/jest.setup.js"],
  testEnvironment: "jsdom",
  testMatch: ["<rootDir>src/**/*.test.(ts|tsx|js)"],
  testPathIgnorePatterns: ["<rootDir>/node_modules/", "<rootDir>/.next/"],
  transform: { "^.+\\.(js|jsx|ts|tsx)$": ["babel-jest", { presets: ["next/babel"] }] },
  transformIgnorePatterns: ["/node_modules/"],
};
