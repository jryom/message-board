# Simple Forum

This is a [NextJS](https://nextjs.org/) app that implements a rudimentary public forum. The simple
styling is done with [TailWindCSS](tailwindcss.com/).

[msw](https://mswjs.io/) is used for intercepting HTTP requests to a non-existent backend. Unit
tests are done with [Jest](https://jestjs.io/) and [Testing Library](https://testing-library.com/),
running tests against [Storybook](https://storybook.js.org/) examples. E2E testing is done with
[Cypress](https://www.cypress.io/).

## Prerequisites

The project uses [`yarn`](https://classic.yarnpkg.com/lang/en/) for dependency management.

## Getting started

1. Run `git clone https://gitlab.com/jryom/message-board.git`
2. Run `yarn install` in local repository folder

Now you can:

- Run local development server with `yarn dev` (on https://localhost:3000)
- Run Storybook with `yarn storybook` (on https://localhost:4000)
- Lint code with `yarn lint`
- Format code with `yarn format`
- Run tests with `yarn test`
- Run a production build with `yarn build`
