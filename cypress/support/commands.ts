import "@testing-library/cypress/add-commands";
import { labels } from "../../src/constants/labels";

declare global {
  namespace Cypress {
    interface Chainable {
      login(): void;
    }
  }
}

Cypress.Commands.add("login", () => {
  cy.findByLabelText(labels.PICK_USERNAME).type("Username{enter}");
});
