import "./commands";

beforeEach(() => {
  cy.window().then((win) => {
    win.sessionStorage.clear();
  });

  cy.visit("/");
});
