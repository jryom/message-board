import { labels } from "../../src/constants/labels";

context("index page", () => {
  it("should be able to visit the index page and enter a username", () => {
    cy.login();
  });

  it("should be able to visit a thread and return to index", () => {
    cy.login();
    cy.findAllByRole("link", { name: labels.VIEW_THREAD }).first().click();
    cy.findByRole("heading", { name: labels.THREAD });
    cy.go("back");
    cy.findAllByRole("link", { name: labels.VIEW_THREAD }).should("have.length", 5);
  });
});
