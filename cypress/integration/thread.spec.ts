import { labels } from "../../src/constants/labels";

const message = "This is a new thread";

context("thread", () => {
  it("should be able to create a new thread", () => {
    cy.login();
    cy.findByRole("textbox").type(message + "{enter}");
    cy.findByText(message).siblings().contains(labels.VIEW_THREAD).click();
    cy.findByText(message);
  });

  it("should be able to delete a thread", () => {
    cy.login();
    cy.findByRole("textbox").type(message + "{enter}");
    cy.findByText(message).siblings().contains(labels.VIEW_THREAD).click();
    cy.findByText(labels.DELETE).click();
    cy.findByText(labels.DELETED_MESSAGE);
  });
});
