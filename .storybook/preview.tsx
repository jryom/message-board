import { addDecorator } from "@storybook/react";
import { MessagesProvider } from "../src/contexts/messages-context";
import { UsernameProvider } from "../src/contexts/username-context";

import "../src/styles/globals.css";

export const parameters = {
  actions: { argTypesRegex: "^on.*" },
};

export const loaders = [
  async () => ({
    messageStore: await fetch("/messages").then((res) => res.json()),
  }),
];

export const decorators = [
  (story, { loaded: { messageStore } }) => (
    <UsernameProvider initial="Username">
      <MessagesProvider initial={messageStore}>{story()}</MessagesProvider>
    </UsernameProvider>
  ),
];

if (typeof global.process === "undefined") {
  const { worker } = require("../src/mocks/browser");

  worker.start({ onUnhandledRequest: "bypass" });
}
