import "../styles/globals.css";

import { Auth } from "../components/auth/auth";
import { MessagesProvider } from "../contexts/messages-context";
import { UsernameProvider } from "../contexts/username-context";

if (process.env.NEXT_PUBLIC_API_MOCKING === "enabled") {
  require("../mocks");
}

export default function App({ Component, pageProps }) {
  return (
    <UsernameProvider>
      <MessagesProvider>
        <Auth />
        <Component {...pageProps} />
      </MessagesProvider>
    </UsernameProvider>
  );
}
