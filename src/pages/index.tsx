import { useEffect } from "react";

import { labels } from "../constants/labels";
import { useMessages } from "../contexts/messages-context";
import { Posts } from "../modules/posts/posts";

const Home = (props) => {
  const { messages, setMessages } = useMessages();

  useEffect(() => {
    if (!messages.length) setMessages(props.messages);
  }, [messages, setMessages, props.messages]);

  return (
    <div className="prose prose-sm sm:prose m-auto pb-10 pt-10">
      <h1 className="box mb-0 text-center font-mono">{labels.HEADER}</h1>
      <div>
        <Posts />
      </div>
    </div>
  );
};

export async function getServerSideProps() {
  const res = await fetch("http://localhost:3000/messages");
  const messages = await res.json();

  return {
    props: {
      messages,
    },
  };
}

export default Home;
