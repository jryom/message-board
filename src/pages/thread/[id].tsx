import { useRouter } from "next/router";
import { useEffect } from "react";

import { labels } from "../../constants/labels";
import { useMessages } from "../../contexts/messages-context";
import { Thread } from "../../modules/thread/thread";

const ThreadPage = (props) => {
  const { id } = useRouter().query;
  const { messages, setMessages } = useMessages();

  useEffect(() => {
    if (!messages.length) setMessages(props.messages);
  }, [messages, setMessages, props.messages]);

  return (
    <div className="prose prose-sm sm:prose m-auto pb-10 pt-10">
      <h1 className="box mb-0 text-center font-mono">{labels.THREAD}</h1>
      <Thread parentId={Number(id)} />
    </div>
  );
};

export async function getServerSideProps() {
  const res = await fetch("http://localhost:3000/messages");
  const messages = await res.json();

  return {
    props: {
      messages,
    },
  };
}

export default ThreadPage;
