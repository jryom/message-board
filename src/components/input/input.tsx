import { useState } from "react";

import { labels } from "../../constants/labels";
import { useMessages } from "../../contexts/messages-context";
import { useUsername } from "../../contexts/username-context";

export const Input = ({
  placeholder,
  parentId = null,
  onSubmit,
  initialValue,
  label,
}: {
  placeholder: string;
  parentId: number | null;
  onSubmit: (message) => Promise<number>;
  initialValue?: string;
  label: string;
}) => {
  const { username } = useUsername();
  const [inputValue, setInputValue] = useState(initialValue);
  const { messages } = useMessages();
  const [statusCode, setStatusCode] = useState(null);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const reqBody = {
      message: inputValue,
      author: username,
      parentId,
      id: messages.length,
    };

    const status = await onSubmit(reqBody);

    setStatusCode(status);

    if (status === 200) {
      event.target.reset();
    }
  };

  const handleChange = ({ target: { value } }) => {
    setInputValue(value);
  };

  const hasError = statusCode >= 300;
  const wasSubmitted = statusCode === 200;

  return (
    <form className="mt-8" onSubmit={handleSubmit}>
      <input
        aria-label={label}
        className="border-1 block w-full font-sans border-2 border-gray-300 focus:border-indigo-300 rounded border-collapse shadow-sm focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
        defaultValue={initialValue}
        onChange={handleChange}
        placeholder={placeholder}
        type="text"
      />

      {wasSubmitted ? (
        <div className="mt-1 p-3 pl-5 pr-5 bg-green-100 rounded">
          <span>{labels.SUBMISSION_SUCCESSFUL}</span>
        </div>
      ) : null}

      {hasError ? (
        <div className="mt-1 p-3 pl-5 pr-5 bg-red-100 rounded">
          <span>{labels.SUBMISSION_FAILED}</span>
        </div>
      ) : null}
    </form>
  );
};
