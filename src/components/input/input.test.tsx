import { labels } from "../../constants/labels";
import { render, screen, specialChars, userEvent, waitFor } from "../../utils/test-utils";
import { Default } from "./input.stories";

it("submits a message", async () => {
  const onSubmit = jest.fn();

  render(<Default onSubmit={onSubmit} />);

  userEvent.type(screen.getByRole("textbox"), `User can type and submit ${specialChars.enter}`);

  await waitFor(() => expect(onSubmit).toHaveBeenCalled());
});

it("displays a successful submission", async () => {
  const onSubmit = jest.fn().mockReturnValue(300);

  render(<Default onSubmit={onSubmit} />);

  userEvent.type(screen.getByRole("textbox"), `User can type and submit ${specialChars.enter}`);

  await waitFor(() => expect(onSubmit).toHaveBeenCalled());

  screen.findByText(labels.SUBMISSION_SUCCESSFUL);
});

it("displays an error", async () => {
  const onSubmit = jest.fn().mockReturnValue(300);

  render(<Default onSubmit={onSubmit} />);

  userEvent.type(screen.getByRole("textbox"), `User can type and submit ${specialChars.enter}`);

  await waitFor(() => expect(onSubmit).toHaveBeenCalled());

  screen.findByText(labels.SUBMISSION_FAILED);
});
