import { Input } from "./input";

export default {
  component: Input,
  title: "Components / Input",
};

export const Default = (args) => <Input {...args} />;
Default.args = { placeholder: "This is a placeholder", label: "This is a label" };

export const WithInitialValue = (args) => <Input {...args} />;
WithInitialValue.args = { ...Default.args, initialValue: "This is an initial value" };
