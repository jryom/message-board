import { labels } from "../../constants/labels";
import { render, screen, userEvent } from "../../utils/test-utils";
import { Default } from "./text.stories";

const message = "This is a message";
const author = "Username";
const id = 123;

it("displays message and author", () => {
  render(<Default author={author} message={message} />);

  screen.getByText(message);
  screen.getByText(author);
});

it("links to thread", () => {
  render(<Default author={author} id={id} message={message} />);

  const link = screen.getByRole("link");
  expect(link).toHaveAttribute("href", `/thread/${id}`);
});

it("handles edit", () => {
  const editHandler = jest.fn();
  render(
    <Default
      author={author}
      handleDelete={() => {}}
      handleEdit={editHandler}
      id={id}
      inThread
      message={message}
    />
  );

  const link = screen.getByRole("button", { name: labels.EDIT });
  userEvent.click(link);
  expect(editHandler).toHaveBeenCalled();
});

it("handles delete", () => {
  const deleteHandler = jest.fn();
  render(
    <Default
      author={author}
      handleDelete={deleteHandler}
      handleEdit={() => {}}
      id={id}
      inThread
      message={message}
    />
  );

  const link = screen.getByRole("button", { name: labels.DELETE });
  userEvent.click(link);
  expect(deleteHandler).toHaveBeenCalled();
});
