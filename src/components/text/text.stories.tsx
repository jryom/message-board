import { hacker, internet } from "faker";

import { Text } from "./text";

export default {
  component: Text,
  title: "Components / Text",
};

export const Default = (args) => <Text {...args} />;

Default.args = { message: hacker.phrase(), author: internet.userName() };
