import Link from "next/link";

import { labels } from "../../constants/labels";

export type TextProps = {
  author?: string;
  handleDelete?: () => void;
  handleEdit?: () => void;
  id: number;
  inThread: boolean;
  message: string;
  parentId: number | null;
};

export const Text = ({
  author,
  handleDelete,
  handleEdit,
  id,
  inThread,
  message,
  parentId,
}: TextProps) => {
  const isPost = typeof parentId !== "number";

  return (
    <div
      className={`p-3 sm:p-5 font-sans even:bg-gray-50 border-b-2 border-solid border-black${
        !author ? " italic" : ""
      }${inThread ? "" : " first:border-t-2"}`}
    >
      <p className="!mt-0 !mb-8 sm:!mb-10">{message}</p>
      <div className="flex flex-auto items-center justify-between w-full">
        {author ? (
          <p className="prose-sm !m-0 block text-right text-gray-600 italic">{author}</p>
        ) : null}
        {handleDelete && handleEdit ? (
          <div className="prose-sm bold">
            <button
              className="!mb-0 mr-2 p-2 pt-1 text-xs bg-red-100 hover:bg-red-300 rounded transition-colors sm:mr-4 sm:text-base"
              onClick={handleDelete}
              type="button"
            >
              {labels.DELETE}
            </button>
            <button
              className="!mb-0 p-2 pt-1 text-xs bg-yellow-200 hover:bg-yellow-400 rounded transition-colors sm:text-base"
              onClick={handleEdit}
              type="button"
            >
              {labels.EDIT}
            </button>
          </div>
        ) : null}

        {isPost && !inThread ? (
          <Link href={`/thread/${id}`}>
            <a className="block ml-auto">
              <span>{labels.VIEW_THREAD}</span>
            </a>
          </Link>
        ) : null}
      </div>
    </div>
  );
};
