import { useEffect, useState } from "react";

import { labels } from "../../constants/labels";
import { useUsername } from "../../contexts/username-context";

export const Auth = () => {
  const { username, setUsername } = useUsername();
  const [input, setInput] = useState();

  useEffect(() => {
    if (typeof window !== "undefined") {
      const storedUsername = window.sessionStorage.getItem("username");
      if (typeof username === "undefined" && typeof storedUsername === "string")
        setUsername(storedUsername);
    }
    if (typeof username === "string") window.sessionStorage.setItem("username", username);
  }, [username, setUsername]);

  const handleSubmit = () => setUsername(input);
  const handleChange = ({ target: { value } }) => setInput(value);

  return !username ? (
    <div className="fixed min-w-full min-h-full bg-gray-300 bg-opacity-80">
      <div className="absolute left-1/2 top-1/3 pb-24 pl-20 pr-20 pt-24 bg-gray-50 rounded -translate-x-1/2 -translate-y-1/2 drop-shadow-2xl">
        <form onSubmit={handleSubmit}>
          <input
            aria-label={labels.PICK_USERNAME}
            className="border-1 block w-full font-sans border-2 border-gray-300 focus:border-indigo-300 rounded-none border-collapse shadow-sm focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
            onChange={handleChange}
            placeholder={labels.PICK_USERNAME}
            type="text"
          />
        </form>
      </div>
    </div>
  ) : null;
};
