import React from "react";

import { UsernameProvider } from "../../contexts/username-context";
import { Auth } from "./auth";

export default {
  component: Auth,
  title: "Components / Auth",
};

export const Default = () => {
  window.sessionStorage.removeItem("username");
  return <Auth />;
};
Default.decorators = [
  (story) => <UsernameProvider initial={undefined}>{story()}</UsernameProvider>,
];

export const WithUsernameInContext = () => {
  window.sessionStorage.removeItem("username");
  return <Auth />;
};
WithUsernameInContext.decorators = [
  (story) => <UsernameProvider initial="Username">{story()}</UsernameProvider>,
];

export const WithUsernameInSessionStorage = () => {
  window.sessionStorage.setItem("username", "username");
  return <Auth />;
};
WithUsernameInSessionStorage.decorators = [
  (story) => <UsernameProvider initial={undefined}>{story()}</UsernameProvider>,
];
