import { labels } from "../../constants/labels";
import { UsernameProvider } from "../../contexts/username-context";
import { originalRender, screen, specialChars, userEvent, waitFor } from "../../utils/test-utils";
import { Default } from "./auth.stories";

let setItem;
let getItem;

const username = "SomeUser";

beforeEach(() => {
  setItem = jest.spyOn(Storage.prototype, "setItem");
  getItem = jest.spyOn(Storage.prototype, "getItem");
});

it("shows modal when no username is stored", () => {
  originalRender(
    <UsernameProvider initial={undefined}>
      <Default />
    </UsernameProvider>
  );

  expect(getItem).toHaveBeenCalledWith("username");

  screen.getByRole("textbox", { name: labels.PICK_USERNAME });
});

it("can store a username", async () => {
  originalRender(
    <UsernameProvider initial={undefined}>
      <Default />
    </UsernameProvider>
  );

  const input = screen.getByRole("textbox", { name: labels.PICK_USERNAME });
  userEvent.type(input, `${username}${specialChars.enter}`);

  expect(setItem).toHaveBeenCalledWith("username", username);

  await waitFor(() => {
    expect(input).not.toBeInTheDocument();
  });
});

it("is hidden when username is stored in contex", () => {
  originalRender(
    <UsernameProvider initial={username}>
      <Default />
    </UsernameProvider>
  );

  const input = screen.queryByRole("textbox", { name: labels.PICK_USERNAME });

  expect(input).not.toBeInTheDocument();
});

it("is hidden when username is stored in session storage", () => {
  getItem.mockReturnValue(username);

  originalRender(
    <UsernameProvider initial={undefined}>
      <Default />
    </UsernameProvider>
  );

  const input = screen.queryByRole("textbox", { name: labels.PICK_USERNAME });

  expect(input).not.toBeInTheDocument();
});
