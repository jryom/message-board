import { useState } from "react";

import { Input } from "../../components/input/input";
import { Text } from "../../components/text/text";
import { labels } from "../../constants/labels";
import { useMessages } from "../../contexts/messages-context";
import { useUsername } from "../../contexts/username-context";

export type MessageProps = {
  author?: string;
  id: number;
  inThread?: boolean;
  message: string;
  parentId: number | null;
};

export const Message = ({ author, id, inThread, message, parentId }: MessageProps) => {
  const { username } = useUsername();
  const [editMode, setEditMode] = useState(false);
  const canEdit = author === username;

  const { messages, setMessages } = useMessages();

  const handleDelete = async () => {
    const { status } = await fetch(`/messages/${id}`, { method: "DELETE" });

    if (status === 204)
      setMessages(
        messages.map((msg) =>
          msg.id === id ? { ...msg, message: labels.DELETED_MESSAGE, author: null } : msg
        )
      );
  };

  const handleEdit = async () => {
    setEditMode(true);
  };

  const handleEditSubmit = async (body) => {
    const { status } = await fetch(`/messages/${id}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
      },
      body: JSON.stringify({ ...body, id, parentId }),
    });

    const otherMessages = messages.filter((msg) => msg.id !== id);
    const oldMessage = messages.find((msg) => msg.id === id);

    setEditMode(false);

    setMessages([...otherMessages, { ...oldMessage, ...body, id, parentId }]);

    return status;
  };

  const renderInput = (
    <Input
      initialValue={message}
      label={labels.EDIT_COMMENT_LABEL}
      onSubmit={handleEditSubmit}
      parentId={parentId}
      placeholder={labels.EDIT_COMMENT_LABEL}
    />
  );

  const renderText = (
    <Text
      author={author}
      id={id}
      inThread={inThread}
      message={message}
      parentId={parentId}
      {...(canEdit && {
        handleDelete,
        handleEdit,
      })}
    />
  );

  return editMode ? renderInput : renderText;
};
