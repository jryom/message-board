import { Input } from "../../components/input/input";
import { labels } from "../../constants/labels";
import { useMessages } from "../../contexts/messages-context";
import { Message, MessageProps } from "../message/message";

const recursiveFindReplies = (id: number, messages: MessageProps[]): MessageProps[] => {
  let nextId;

  return [
    messages.find((message) => {
      if (message.parentId === id) {
        nextId = message.id;
        return true;
      }
      nextId = undefined;
      return false;
    }),
    ...(typeof nextId === "number" ? recursiveFindReplies(nextId, messages) : []),
  ].filter(Boolean);
};

export const Thread = ({ parentId }: { parentId: number }) => {
  const { messages, setMessages } = useMessages();

  if (!messages.length) return null;

  const parent = messages.find((message) => Number(message.id) === parentId);

  const replies = recursiveFindReplies(parentId, messages);

  const handleSubmit = async (reqBody) => {
    const res = await fetch("/messages", {
      body: JSON.stringify(reqBody),
      method: "POST",
      headers: {
        Accept: "application/json",
      },
    });

    if (res.status === 200) {
      setMessages([...messages, reqBody]);
    }

    return res.status;
  };

  return (
    <>
      <Message {...parent} inThread />
      {replies.map((message) => (
        <Message key={message.id} {...message} />
      ))}
      <Input
        label={labels.NEW_COMMENT_PLACEHOLDER}
        onSubmit={handleSubmit}
        parentId={replies[replies.length - 1]?.id || parent.id}
        placeholder={labels.NEW_COMMENT_PLACEHOLDER}
      />
    </>
  );
};
