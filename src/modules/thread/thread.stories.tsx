import React from "react";

import { Thread } from "./thread";

const storybookSettings = {
  component: Thread,
  title: "Modules / Thread",
};

export default storybookSettings;

export const Default = (args) => <Thread {...args} />;
Default.args = { parentId: 0 };
