import { labels } from "../../constants/labels";
import { MessagesProvider } from "../../contexts/messages-context";
import { UsernameProvider } from "../../contexts/username-context";
import { createMockThreads } from "../../mocks/generators/mock-thread";
import {
  act,
  originalRender,
  screen,
  specialChars,
  userEvent,
  waitFor,
} from "../../utils/test-utils";
import { Default } from "./thread.stories";

let mockThread;

const threads = 1;
const replies = 5;
const username = "SomeUser";
const mockMessage = "Some message";

beforeAll(() => {
  mockThread = createMockThreads(threads, replies);
});

it("renders thread", () => {
  originalRender(
    <UsernameProvider initial={username}>
      <MessagesProvider initial={mockThread}>
        <Default parentId={0} />
      </MessagesProvider>
    </UsernameProvider>
  );

  mockThread.forEach((message) => {
    screen.getByText(message.message);
  });
});

it("can create new reply", async () => {
  originalRender(
    <UsernameProvider initial={username}>
      <MessagesProvider initial={mockThread}>
        <Default parentId={0} />
      </MessagesProvider>
    </UsernameProvider>
  );

  const input = screen.getByRole("textbox");

  userEvent.type(input, mockMessage + specialChars.enter);

  await screen.findByText(mockMessage);
});

it("can delete reply", async () => {
  const { author, message } = mockThread[mockThread.length - 1];

  originalRender(
    <UsernameProvider initial={author}>
      <MessagesProvider initial={mockThread}>
        <Default parentId={0} />
      </MessagesProvider>
    </UsernameProvider>
  );

  const buttonDelete = screen.getByRole("button", { name: labels.DELETE });

  expect(screen.queryByText(message)).toBeInTheDocument();

  userEvent.click(buttonDelete);

  await waitFor(() => {
    expect(screen.queryByText(message)).toBeNull();
  });

  screen.findByText(labels.DELETED_MESSAGE);
});

it("can edit reply", async () => {
  const { author, message } = mockThread[mockThread.length - 1];

  originalRender(
    <UsernameProvider initial={author}>
      <MessagesProvider initial={mockThread}>
        <Default parentId={0} />
      </MessagesProvider>
    </UsernameProvider>
  );

  const buttonEdit = screen.getByRole("button", { name: labels.EDIT });

  expect(screen.queryByText(message)).toBeInTheDocument();

  userEvent.click(buttonEdit);

  const input = screen.getByLabelText(labels.EDIT_COMMENT_LABEL);

  expect(input).toHaveValue(message);

  userEvent.type(input, mockMessage + specialChars.enter);

  jest.spyOn(console, "error").mockImplementation(() => {});

  await waitFor(() => screen.getByText(message + mockMessage));
});
