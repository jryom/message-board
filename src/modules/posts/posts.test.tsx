import { labels } from "../../constants/labels";
import { MessagesProvider } from "../../contexts/messages-context";
import { UsernameProvider } from "../../contexts/username-context";
import { createMockThreads } from "../../mocks/generators/mock-thread";
import { originalRender, screen, specialChars, userEvent } from "../../utils/test-utils";
import { Default } from "./posts.stories";

let mockThreads;

const threads = 4;
const replies = 5;
const username = "SomeUser";
const mockMessage = "Some message";

beforeAll(() => {
  mockThreads = createMockThreads(threads, replies);
});

it("renders all threads", () => {
  originalRender(
    <UsernameProvider initial={username}>
      <MessagesProvider initial={mockThreads}>
        <Default />
      </MessagesProvider>
    </UsernameProvider>
  );

  const threadCount = screen.queryAllByText(labels.VIEW_THREAD);

  expect(threadCount).toHaveLength(threads);
});

it("can create new thread", async () => {
  originalRender(
    <UsernameProvider initial={username}>
      <MessagesProvider initial={mockThreads}>
        <Default />
      </MessagesProvider>
    </UsernameProvider>
  );

  const textbox = screen.getByRole("textbox");

  userEvent.type(textbox, `${mockMessage}${specialChars.enter}`);

  await screen.findByText(mockMessage);
  await screen.findByText(username);
});
