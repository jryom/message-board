import React from "react";

import { Posts } from "./posts";

const storybookSettings = {
  component: Posts,
  title: "Modules / Posts",
};

export default storybookSettings;

export const Default = () => <Posts />;
