import { Input } from "../../components/input/input";
import { Text } from "../../components/text/text";
import { labels } from "../../constants/labels";
import { useMessages } from "../../contexts/messages-context";

export const Posts = () => {
  const { messages, setMessages } = useMessages();

  const posts = messages.filter((msg) => msg.parentId === null);

  const handleSubmit = async (reqBody) => {
    const res = await fetch("/messages", {
      body: JSON.stringify(reqBody),
      method: "POST",
      headers: {
        Accept: "application/json",
      },
    });

    if (res.status === 200) {
      setMessages([...messages, reqBody]);
    }

    return res.status;
  };

  return (
    <>
      {posts.map((post) => (
        <Text key={post.id} {...post} />
      ))}
      <Input
        label={labels.NEW_POST_PLACEHOLDER}
        onSubmit={handleSubmit}
        parentId={null}
        placeholder={labels.NEW_POST_PLACEHOLDER}
      />
    </>
  );
};
