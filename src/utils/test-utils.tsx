import { render as originalRender } from "@testing-library/react";
import * as React from "react";

import { MessagesProvider } from "../contexts/messages-context";
import { UsernameProvider } from "../contexts/username-context";
import { createMockThreads } from "../mocks/generators/mock-thread";

const render = async (node: React.ReactNode) =>
  originalRender(
    <UsernameProvider initial="Username">
      <MessagesProvider initial={createMockThreads(5, 5)}>{node}</MessagesProvider>
    </UsernameProvider>
  );

export * from "@testing-library/react";
export { specialChars, default as userEvent } from "@testing-library/user-event";
export { originalRender, render };
