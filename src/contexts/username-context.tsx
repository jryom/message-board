import { createContext, useContext, useState } from "react";

const UsernameContext = createContext(undefined);

export const UsernameProvider = ({ children, initial }: { children: any; initial?: string }) => {
  const [username, setUsername] = useState(initial);

  return (
    <UsernameContext.Provider value={{ username, setUsername }}>
      {children}
    </UsernameContext.Provider>
  );
};

export const useUsername = () => useContext(UsernameContext);
