import { createContext, useContext, useState } from "react";

import { MessageProps } from "../modules/message/message";

const MessagesContext = createContext(null);

export const MessagesProvider = ({
  children,
  initial = [],
}: {
  children: any;
  initial?: MessageProps[];
}) => {
  const [messages, setMessages] = useState(initial);

  return (
    <MessagesContext.Provider value={{ messages, setMessages }}>
      {children}
    </MessagesContext.Provider>
  );
};

export const useMessages = () => useContext(MessagesContext);
