export const labels = {
  DELETE: "Delete",
  DELETED_MESSAGE: "This message was deleted by the author.",
  EDIT: "Edit",
  EDIT_COMMENT_LABEL: "Type to edit comment",
  HEADER: "Best Forum Ever",
  NEW_COMMENT_PLACEHOLDER: "Type to write a comment...",
  NEW_POST_PLACEHOLDER: "Type to start a new thread...",
  PICK_USERNAME: "Please pick a username",
  SUBMISSION_FAILED: "There was an error with your submission, please try again later.",
  SUBMISSION_SUCCESSFUL: "Message submitted successfully.",
  THREAD: "Thread",
  VIEW_THREAD: "View thread",
};
