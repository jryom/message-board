import { rest } from "msw";

import { labels } from "../constants/labels";
import { createMockThreads } from "./generators/mock-thread";

const getOrCreateThreads = () =>
  global.mockMessages ||
  (() => {
    const messages = createMockThreads(5, 5);

    global.mockMessages = messages;

    return messages;
  })();

const getMessages = rest.get("*/messages", (req, res, ctx) => res(ctx.json(getOrCreateThreads())));

const postMessage = rest.post("*/messages", (req, res, ctx) => {
  const body = JSON.parse(req.body as any);

  const mockMessages = getOrCreateThreads();

  body.id = mockMessages.length;

  global.mockMessages = [...mockMessages, body];

  return res(ctx.status(200));
});

const putMessage = rest.put("/messages/:id", (req, res, ctx) => {
  const body = JSON.parse(req.body as any);

  const mockMessages = getOrCreateThreads();
  const otherMessages = mockMessages.filter((msg) => msg.id !== Number(req.params.id));
  const oldMessage = mockMessages.find((msg) => msg.id === Number(req.params.id));

  global.mockMessages = [...otherMessages, { ...oldMessage, ...body }];

  return res(ctx.status(200));
});

// TODO: Recursively delete all replies
const deleteMessage = rest.delete("/messages/:id", (req, res, ctx) => {
  global.mockMessages = global.mockMessages.map((msg) =>
    msg.id === Number(req.params.id)
      ? { ...msg, message: labels.DELETED_MESSAGE, author: null }
      : msg
  );

  return res(ctx.status(204));
});

export const handlers = [getMessages, postMessage, deleteMessage, putMessage];
