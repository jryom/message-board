import faker from "faker";

export const createMockMessage = (parentId = null, id = 0) => ({
  author: faker.internet.userName(),
  id,
  message: faker.hacker.phrase(),
  parentId,
});
