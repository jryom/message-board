import { MessageProps } from "../../modules/message/message";
import { createMockMessage } from "./mock-message";

export const createMockThreads = (threads = 1, length = 10): MessageProps[] => {
  const messages = [];

  for (let thread = 0; thread < threads; thread++) {
    messages.push(createMockMessage(null, messages.length));
    for (let items = 0; items < length; items++) {
      messages.push(createMockMessage(messages.length - 1, messages.length));
    }
  }

  return messages;
};
