Design and build a “single page application” to implement a simple public message board app with the
following features:

- A client can create a new public message thread
- A client can add a public reply to a message thread
- A client can modify their own messages
- A client can delete their own messages
- Public message threads and replies are viewable

See the “Mock API” below for a sketch of the backend this app would communicate with. You are
welcome to mock this connection with your preferred technique, and to fill in any gaps in the
specification with your own thoughts. All such decisions form part of the discussion and help us
understand how you view code.

## Technical Details

We suggest using React, but will happily proceed with your preferred choice if you have other
preferences. The complete solution should include a README file that describes how to build and run
your project. Please share it through a public source code repository or via email in a single zip
file. Optionally, add tests to suitable sections of your code and prepare to discuss your view on
testing web applications.
https://docs.google.com/document/d/1JGOYB7icpnIiIivi_zKvLVuS-9ucE4f1s_PtqWftyPU/edit# 1/3

### Mock API

List all messages in the system. There is no paging or threading in the response body.

**GET** `/messages`: 200

_ResponseBody_:

```JSON
{
  "messages": [
    {
      "id": 1,
      "message": "A Thread",
      "parentId": null,
      "author": 1
    },
    {
      "id": 2,
      "message": "A Reply",
      "parentId": 1,
      "author": 1
    }
  ]
}
```

### Post a new thread or reply

**POST** `/messages`: 204

_RequestBody_

```JSON
{
  "message": "A Thread",
  "parentId": null,
  "author": 1
}
```

### Update a thread or reply

**PUT** `/messages/{id}`: 204

_RequestBody_

```JSON
{
  "message": "A Thread"
}
```

### Remove a thread or reply

**DELETE** `/messages/{id}`: 204
